/**
 * Created by packyag on 3/5/18.
 */


function changePwdVisibility(cntntPwd, pwdBox1, pwdBox2) {
    var $pwdContentHolder = $("#" + cntntPwd), $pwdBox;
    if (pwdBox2) {
        $pwdBox = $("#" + pwdBox1 + ", " + "#" + pwdBox2);
    } else {
        $pwdBox = $("#" + pwdBox1)
    }
    if ($pwdContentHolder.hasClass('fa-eye')) {
        $pwdContentHolder.removeClass('fa-eye');
        $pwdContentHolder.addClass('fa-eye-slash');
        $pwdBox.attr('type', 'password');
    } else if ($pwdContentHolder.hasClass('fa-eye-slash')) {
        $pwdContentHolder.removeClass('fa-eye-slash');
        $pwdContentHolder.addClass('fa-eye');
        $pwdBox.attr('type', 'text');
    }
}

function loginToTmatics(){

    var $v_email = $("#v-email");
    $(".login_fb_alert").hide();
    $v_email.hide();


    var email = $.trim($("#loginEmail").val());
    var password = $.trim($("#loginPwd").val());
    var window_width = window.innerWidth;
    if(isEmpty(email) && isEmpty(password)){
        if(window_width > 767)
            $("#noMailAndPwd").show();
        else {
            $v_email.show();
        }
    }else if(isEmpty(email)){
        if(window_width > 767)
            $("#noMail").show();
        else {
            $v_email.show();
        }
    }
    else if(isEmpty(password)){
        if(window_width > 767)
            $("#noPwd").show();
        else {
            $v_email.show();
        }
    }else{
        window.location.href = '/reports';
        return
        $("#loggingInBtn").html('<i class="fa fa fa-spinner fa-spin" style="color:#fff"></i>&nbsp;&nbsp;  Login to Waterwerx').attr('disabled',true);
        var data = {
            'username' : $.trim($("#loginEmail").val()),
            'password': $.trim($("#loginPwd").val()),
            timeZone : moment.tz.guess()
        };

        //tzAjax('http://10.1.10.52:8000/v1/login','POST',data, function(result){
        tzAjax(apiUrl+'/v1/login','POST',data, function(result){
            $("#loggingInBtn").html('<i class="fa fa-lock" aria-hidden="true" style="color:#fff"></i>&nbsp;&nbsp;Login to Waterwerx').attr('disabled',false);

            if(result && result.users && result.users.status == 'SUCCESS'){

                $.cookie.json = true;
                $.cookie('userObj', result.users);
                $.cookie('sid',result.users.sid);
                $.cookie('accountId',result.users.accountId);
                $.cookie('userId',result.users.userId);

                window.location.href = '/reports';

            } else if(result && result.users && result.users.error && result.users.error.code == 'E301') {
                tzlTrack.trackThis("Login - Failed to login",{"module":"Login",data:data, Result : result},true);
                $("#incorrectMail").show();
            } else {
                tzlTrack.trackThis("Login - Failed to login",{"module":"Login",data:data, Result : result},true);
                $("#accessDenied").show();
            }
        }, function(err){
            tzlTrack.trackThis("Ajax Error",{"module":"Login",data:data, Error : JSON.stringify(err),"Url":'/v1/login'},true);
            $("#loggingInBtn").html('<i class="fa fa-lock" aria-hidden="true" style="color:#fff"></i>&nbsp;&nbsp;Login to Waterwerx').attr('disabled',false);
            $("#accessDenied").show();
        })



    }


    //window.location.href = '/'


}



function loggingOut(){
    var data = {
        userId : JSON.parse($.cookie('userObj')).userId,
        sid :JSON.parse($.cookie('userObj')).sid
    };
    tzAjax(apiUrl+'/logout',"POST", data, function(result){
        if(result && result.users &&  result.users.status &&  result.users.status == "SUCCESS"){
            //tzlTrack.trackThis("Logout - User logout successful",{module : 'Logout', data : data}, true);
            $.cookie('userObj','');
            $.cookie('sid','');
            $.cookie('userId','');
            $.cookie('accountId','');
            window.location.href = '/login';
        } else {
            //tzlTrack.trackThis("Logout - User logout failed",{module : 'Logout', data : data, result : result}, true);
            window.location.href = '/login';
        }

    }, function(err){
        //tzlTrack.trackThis("Ajax Error",{"module":"Event",Error : JSON.stringify(err),data : data,"Url":'/v1/logout'},true);
        window.location.href = '/login';

    })
}