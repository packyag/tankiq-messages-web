//Script for daily report grid //



$(function () {
 nProgressLoad();
    $("#customId").datepicker();

    $("#snap-drepotlist li a").click(function(){

        $("#snap-dreport-drop span:first-child").html($(this).text());

    });

    $("#snap-histlist li a").click(function(){

        $("#snap-hist-drop span:first-child").html($(this).text());

    });



    loadChart();
// var options;
    // $('#daterange').daterangepicker(options, function(start, end, label) { console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')'); });

});


    function loadDailyRpt() {
        nProgressLoad();
        var tableObj = {
            targetId: '#snapDlyRptTargetId',
            templateId: "#snapDlyRptTemplateId",
            url: 'snapshot/history-grid', // API call URL
//                                                    url: 'http://tankiq.tmatics.com/asset/assetstatushistory?asset_id=58ec83be71074fbc4d000002&page_number=1&records_per_page=10&sort%5B0%5D%5BcolumnName%5D=messageTime&sort%5B0%5D%5Border%5D=-1&fromDate=&toDate=&dateFilterField=messageTimeMillis', // API call URL
            params: {
                limit: 10,
                offset: 0,
                //searchFor:searchFor,
                //searchValue:searchValue,
                sort: '',
                sortType: 'single',
                sortField: '',
                sortOrder: '',
                filter: {}
            },
            searchOptions: {
                enabled: true,
                filterId: "#filterId",// Dropdown Filter Id (if available)
                inputId: "#searchBox", // Search Input Id ()
                inputClearId: "#inputClearBtn", // Search Clear button Id ()
                searchId: "#searchId", // Search button Id
                autoChange: true, // true - Change Happens based on dropdown change
                enterSearch: true // true - If search while happens while press enter button
            },
            refreshId: "#refreshId",// Refresh button Id
            recperpage: [10, 25, 50, 100],
            hideFooter: false,
            loadingDots: '',
            layout: 'table'
        };


        var table = new tGrid(tableObj, function (result) {
            console.log("Table Drawn");

            
            NProgress.done();

        });
    }


//Script for history grid //


    function loadHistoryData() {
        nProgressLoad();
        var tableObj = {
            targetId: '#snapHistoryTableTargetId',
            templateId: "#snapHistoryTableTemplateId",
            url: 'snapshot/history-grid', // API call URL
//          url: 'http://tankiq.tmatics.com/asset/assetstatushistory?asset_id=58ec83be71074fbc4d000002&page_number=1&records_per_page=10&sort%5B0%5D%5BcolumnName%5D=messageTime&sort%5B0%5D%5Border%5D=-1&fromDate=&toDate=&dateFilterField=messageTimeMillis', // API call URL
            params: {
                limit: 10,
                offset: 0,
                //searchFor:searchFor,
                //searchValue:searchValue,
                sort: '',
                sortType: 'single',
                sortField: '',
                sortOrder: '',
                filter: {}
            },
            searchOptions: {
                enabled: true,
                filterId: "#filterId",// Dropdown Filter Id (if available)
                inputId: "#searchBox", // Search Input Id ()
                inputClearId: "#inputClearBtn", // Search Clear button Id ()
                searchId: "#searchId", // Search button Id
                autoChange: true, // true - Change Happens based on dropdown change
                enterSearch: true // true - If search while happens while press enter button
            },
            refreshId: "#refreshId",// Refresh button Id
            recperpage: [10, 25, 50, 100],
            hideFooter: false,
            loadingDots: '',
            layout: 'table'
        };


        var table = new tGrid(tableObj, function (result) {
            console.log("Table Drawn");
            
            NProgress.done();
        });
    }



//Script for alert grid grid //


    function loadAlertData() {
        nProgressLoad();
        var tableObj = {
            targetId: '#snapAlertTargetId',
            templateId: "#snapAlertTemplateId",
            url: 'snapshot/history-grid', // API call URL
//          url: 'http://tankiq.tmatics.com/asset/assetstatushistory?asset_id=58ec83be71074fbc4d000002&page_number=1&records_per_page=10&sort%5B0%5D%5BcolumnName%5D=messageTime&sort%5B0%5D%5Border%5D=-1&fromDate=&toDate=&dateFilterField=messageTimeMillis', // API call URL
            params: {
                limit: 10,
                offset: 0,
                //searchFor:searchFor,
                //searchValue:searchValue,
                sort: '',
                sortType: 'single',
                sortField: '',
                sortOrder: '',
                filter: {}
            },
            searchOptions: {
                enabled: true,
                filterId: "#filterId",// Dropdown Filter Id (if available)
                inputId: "#searchBox", // Search Input Id ()
                inputClearId: "#inputClearBtn", // Search Clear button Id ()
                searchId: "#searchId", // Search button Id
                autoChange: true, // true - Change Happens based on dropdown change
                enterSearch: true // true - If search while happens while press enter button
            },
            refreshId: "#refreshId",// Refresh button Id
            recperpage: [10, 25, 50, 100],
            hideFooter: false,
            loadingDots: '',
            layout: 'table'
        };


        var table = new tGrid(tableObj, function (result) {
            console.log("Table Drawn");
            
            NProgress.done();
        });
    }


    //chart
    function loadChart() {

     Highcharts.chart('tankLevelChart', {

            title: {
                text: ''
            },
            credits: {
                enabled: false
            },

            subtitle: {
                text: ''
            },

            yAxis: {
                title: {
                    text: 'Tank Level(inches)'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'center',
                verticalAlign: 'bottom'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2010
                }
            },

            series: [{
                name: 'Tank Level',
                data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
        NProgress.done();

    }
    /*Daily gain chart*/


    Highcharts.chart('dailyGainChart', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Gain(inches)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Gain',
            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

        }]

    });


    /*Daily Disposal chart*/


    Highcharts.chart('dailyDisposalChart', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Gain(inches)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Gain',
            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

        }]

    });




$('.btnsnap1').click(function() {
    if ($(this).is("active"))
        $('.btnsnap1').not(this).removeClass('active');
    else
        $(this).addClass('active');
    $('.btnsnap1').not(this).removeClass('active');
});


$('.btnsnap2').click(function() {
    if ($(this).is("active"))
        $('.btnsnap2').not(this).removeClass('active');
    else
        $(this).addClass('active');
    $('.btnsnap2').not(this).removeClass('active');
});
$('.btnsnap-chart1').click(function() {
    if ($(this).is("active"))
        $('.btnsnap-chart1').not(this).removeClass('active');
    else
        $(this).addClass('active');
    $('.btnsnap-chart1').not(this).removeClass('active');
});
$('.btnsnap-chart2').click(function() {
    if ($(this).is("active"))
        $('.btnsnap-chart2').not(this).removeClass('active');
    else
        $(this).addClass('active');
    $('.btnsnap-chart2').not(this).removeClass('active');
});


$('#snapFilter').change(function() {
    var filterData = $(this).val();

    var snapGainTab = $('#gainButtonSnapshot .active').val();

    var url = "snapshot/history-grid?from_date="+filterData+"groupfor="+snapGainTab;

    $.ajax({
        type: "GET",
        url: url,
        // data: '',
        // cache: false,
        success: function(result){
                $("#gainValue").text(result.data.list[0].LiquidQuantity);
            $("#disposalValue").text(result.data.list[0].LiquidQuantity);
            // alert("1"+ data);

        }
    });
    // alert(filterData);

});

function customdate() {

    // $("#dropClose").removeClass('open');
    // alert("hi");
$("#showDate").css({'display':'block'});

    $("#displaydate").datepicker();
     $("#displaydate1").datepicker();

    // $("#datedisplay").show();

}

$(function() {
    $('#displaydate').datepicker({
                defaultDate: "3/11/2018",
            });
    $("#dateRange li").click(function(){
        $("#customDate span:first-child").html($(this).text());
    });

    $("#displaydate").on("change",function(){
        var selected = $(this).val();
    });
    $("#displaydate1").on("change",function(){
        var selected = $(this).val();
        // alert(selected);
    });
});

function applydate() {
    $("#showDate").hide();
    var dateValue = $("#displaydate").datepicker({dateFormat: 'dd,MM,yyyy'}).val();
    var dateValue1 = $("#displaydate1").datepicker({dateFormat: 'dd,MM,yyyy'}).val();
    var text_val = dateValue+'-'+dateValue1;
    $('#customDate span:first-child').html(text_val);
    // alert(text_val);
}
function dateclose() {
    $("#showDate").hide();
}
function showdashboardphotomap(lat , lon , location , tankId){
    map = new google.maps.Map(document.getElementById('imgUploadMap'), {
        center: {lat: lat , lng: lon},
        zoom: 15
    });
    var beachMarker = new google.maps.Marker({
        position: {lat: lat, lng: lon},
        map: map,
        icon: 'images/maps/maptankicon.png'
    });
}
