
$(function(){
    NProgress.start();


    jQuery(window).load(function () {

        NProgress.done();
    });


    $("#summary").show();
    $("#con2").addClass("reference-common-css");


    $("#triggerDiv").click(function() {
        $("#showTrigger").show();
        $('html, body').animate({
            scrollTop: $("#showTrigger").offset().top
        }, 1000);
    });

    $("#tankNotification").click(function() {
        $(".tank-exceeds").show();
        $('html, body').animate({
            scrollTop: $(".tank-exceeds").offset().top
        }, 1000);
    });

    $("#tankLevel").click(function() {

    $("#summary,#mynotifications,#helpnotify").hide();
    $("#createnotify").show();
    $("#con1,#con2,#con3,#con5").removeClass("reference-common-css");
    $("#con4").addClass("reference-common-css");
        $(".tank-exceeds").show();
        $('html, body').animate({
            scrollTop: $(".tank-exceeds").offset().top
        }, 1000);
    });

    $(".summary-tank").click(function() {

        $("#summary,#mynotifications,#helpnotify").hide();
        $("#createnotify").show();
        $("#showTrigger").show();
        $("#con1,#con2,#con3,#con5").removeClass("reference-common-css");
        $("#con4").addClass("reference-common-css");
        $(".tank-exceeds").show();
        $('html, body').animate({
            scrollTop: $(".tank-exceeds").offset().top
        }, 500);
    });

    $(".tank-level-high").click(function() {
        $(".trigger-tank-high").show();
        $('html, body').animate({
            scrollTop: $(".trigger-tank-high").offset().top
        }, 1000);
    });

    $(".battery-notify").click(function() {
        $(".battery").show();
        $('html, body').animate({
            scrollTop: $(".battery").offset().top
        }, 1000);
    });

    $(".batterySummary").click(function() {
        $("#summary,#mynotifications,#helpnotify").hide();
        $("#createnotify").show();
        $("#con1,#con2,#con3,#con5").removeClass("reference-common-css");
        $("#con4").addClass("reference-common-css");

        $(".battery").show();
        $('html, body').animate({
            scrollTop: $(".battery").offset().top
        }, 1000);
    });

    $(".pressure-notify").click(function() {
        $(".pressure").show();
        $('html, body').animate({
            scrollTop: $(".pressure").offset().top
        }, 1000);
    });

    $(".pressureSummary").click(function() {
        $("#summary,#mynotifications,#helpnotify").hide();
        $("#createnotify").show();
        $("#con1,#con2,#con3,#con5").removeClass("reference-common-css");
        $("#con4").addClass("reference-common-css");

        $(".pressure").show();
        $('html, body').animate({
            scrollTop: $(".pressure").offset().top
        }, 1000);
    });


    $(".tank-level-exceeds").click(function() {
         $(".tanklevel-exceeds").show();
        $('html, body').animate({
            scrollTop: $(".tanklevel-exceeds").offset().top
        }, 1000);
    });

    $(".net-daily-disposal").click(function() {
        $(".daily-disposal-exceeds").show();
        $('html, body').animate({
            scrollTop: $(".daily-disposal-exceeds").offset().top
        }, 1000);
    });

    $(".tank-level-low").click(function() {
        $(".trigger-tank-low").show();
        $('html, body').animate({
            scrollTop: $(".trigger-tank-low").offset().top
        }, 1000);
    });

    $(".tank-level-below").click(function() {
        $(".tank-below").show();
        $('html, body').animate({
            scrollTop: $(".tank-below").offset().top
        }, 1000);
    });

    $(".netDaily-gain-exceeds").click(function() {
        $(".net-daily-gain").show();
        $('html, body').animate({
            scrollTop: $(".net-daily-gain").offset().top
        }, 1000);
    });

    $(".netDaily-gain-below").click(function() {
        $(".daily-gain-below").show();
        $('html, body').animate({
            scrollTop: $(".daily-gain-below").offset().top
        }, 1000);
    });

    $(".battery-low").click(function() {
        $(".trigger-battery-low").show();
        $('html, body').animate({
            scrollTop: $(".trigger-battery-low").offset().top
        }, 1000);
    });

    $(".pressure-exceeds").click(function() {
        $(".press-exceeds").show();
        $('html, body').animate({
            scrollTop: $(".press-exceeds").offset().top
        }, 1000);
    });
    $(".pressure-below").click(function() {
        $(".press-below").show();
        $('html, body').animate({
            scrollTop: $(".press-below").offset().top
        }, 1000);
    });

    $(".constraints-btn").click(function() {
        $(".battery-action").show();
        $('html, body').animate({
            scrollTop: $(".battery-action").offset().top
        }, 1000);
    });

    $("#action").click(function() {
        $(".choose-action").show();
        $('html, body').animate({
            scrollTop: $(".choose-action").offset().top
        }, 1000);
    });


    $("#backProcess").click(function() {
        $("#showTrigger").hide();
    });

    $("#backProcessTank").click(function() {
        $(".tank-exceeds").hide();
    });

    $("#backProcessTankHigh").click(function() {
        $(".trigger-tank-high").hide();
    });

    $("#backProcessBattery").click(function() {
        $(".battery").hide();
    });

    $("#backProcessPressure").click(function() {
        $(".pressure").hide();
    });

    $("#backTankExceeds").click(function() {
        $(".tanklevel-exceeds").hide();
    });

    $("#backDailyDisposal").click(function() {
        $(".daily-disposal-exceeds").hide();
    });

    $("#backTankLow").click(function() {
        $(".trigger-tank-low").hide();
    });

    $("#backTankLevel").click(function() {
        $(".tank-below").hide();
    });

    $("#backDailyGain").click(function() {
        $(".net-daily-gain").hide();
    });

    $("#backDailyBelow").click(function() {
        $(".daily-gain-below").hide();
    });
    $("#backBatteryLow").click(function() {
        $(".trigger-battery-low").hide();
    });
    $("#backPressure").click(function() {
        $(".press-exceeds").hide();
    });
    $("#backPressureBelow").click(function() {
        $(".press-below").hide();
    });

    $("#batteryBack").click(function() {
        $(".battery-action").hide();
    });
    $("#backChoose").click(function() {
        $(".choose-action").hide();
    });

});

function summaryNotify() {
    $("#summary").show();
    $("#mynotifications,#createnotify,#helpnotify").hide();
    $("#con2").addClass("reference-common-css");
    $("#con3,#con5,#con1,#con4").removeClass("reference-common-css");
}
function mynotificationNotify() {
    $("#summary,#createnotify,#helpnotify").hide();
    $("#mynotifications").show();
    $("#con1,#con2,#con4,#con5").removeClass("reference-common-css");
    $("#con3").addClass("reference-common-css");
}
function createNotify() {
    $("#summary,#mynotifications,#helpnotify").hide();
    $("#createnotify").show();
    $("#con1,#con2,#con3,#con5").removeClass("reference-common-css");
    $("#con4").addClass("reference-common-css");
}
function helpNotify() {
    $("#summary,#mynotifications,#createnotify").hide();
    $("#helpnotify").show();
    $("#con1,#con2,#con3,#con4").removeClass("reference-common-css");
    $("#con5").addClass("reference-common-css");
}
function createNotification(){
    $("#summary,#mynotifications,#helpnotify").hide();
    $("#createnotify").show();
    $("#con1,#con2,#con3,#con5").removeClass("reference-common-css");
    $("#con4").addClass("reference-common-css");
}
function myNotifications(){
    $("#summary,#createnotify,#helpnotify").hide();
    $("#mynotifications").show();
    $("#con1,#con2,#con4,#con5").removeClass("reference-common-css");
    $("#con3").addClass("reference-common-css");
}
