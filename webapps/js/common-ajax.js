/**
 * Created by packyag on 13/12/17.
 */

function tzAjax(url, method, data, successCbk, errorCbk) {


    $.ajax({

        url: url,
        method: method,
        data: data,

        //dataType: "json",
        success: function (result) {

            if (successCbk && typeof successCbk == "function") {
                successCbk(result)
            }
            else {
                console.log("Error in Ajax success callback")
            }
        },
        error: function(err){
            console.log("Err",err);
            if ( err.responseText === "Session Expired" && err.responseText === "Not in Session") {
            }
            else{
                console.log(err);
                //window.location.href = "/login"
            }
        }

    });
}


