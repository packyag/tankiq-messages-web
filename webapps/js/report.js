function calculateHeight(){
    var windowHeight = $(window).height();
    var domHeight = $(".navbar-fixed-bottom").height() + $(".navbar-inverse").height();
    var calcHeight = windowHeight - (domHeight + 100);
    $(".reportsTab").css({"min-height" : calcHeight+'px'})
}

function initReportsTabs() {
    $('.reportsTab a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

        console.log($(e.target).data('current-tab-name')); // newly activated tab
        console.log($(e.relatedTarget).data('current-tab-name')); // previous active tab

        var cTab = $(e.target).data('current-tab-name');
        var cTabTitle = $.trim($(e.target).text());

        $('.reportsTabTitle').text(cTabTitle);

        switch (cTab){
            case 'Raw Messages':
                loadRawMessagesGrid();
                break;
            case 'Decoded Messages':
                loadDecodedMessagesGrid();
                break;
            default:

                break;
        }
    });
    calculateHeight();
}


function loadRawMessagesGrid(){
    console.log(apiUrl);
    console.log(apiUrl);
    console.log(apiUrl);
    console.log(apiUrl);
    console.log(apiUrl);
    console.log(apiUrl);
    var tableObj = {
        targetId: '#rawMsgTargetId',
        templateId: '#rawMsgTmplId',
        //url: 'http://jl-fitcar-api-test.tmatics.com/v2/device/installation/report',
        url: apiUrl+'/asset/assetLinkmessages',
        params: {
            limit: 10,
            offset: 0,
            searchFor:'',
            searchValue:'',
            sort:[
                {
                    order: -1,
                    columnName:'timestamp'
                }],
            sortField:'',
            sortOrder:'',
            messageType : 'rawMessage',
            version : 'AssetLink_TIQ-200B'
        },
        searchOptions : {
            enabled : true,
            filterId : "#raw-grid-filter",// Dropdown Filter Id (if available)
            inputId : "#searchRawMsgVal", // Search Input Id ()
            inputClearId : "#rawClearBtn", // Search Clear button Id ()
            searchId : "#searchrawMsgBtn", // Search button Id
            autoChange: true, // true - Change Happens based on dropdown change
            enterSearch: true // true - If search while happens while press enter button
        },
        refreshId: "#rawMsgRefresh",
        recperpage: [10, 25, 50,100],
        layout: 'table'
    };
    console.log(tableObj,"=======================");

    var table = new tGrid(tableObj, function (result) {

        $("#totalRawMsgCount").html(result.data && result.data.data && result.data.data.count ? result.data.data.count : '-' )


    });
}



function loadDecodedMessagesGrid(){
    var tableObj = {
        targetId: '#decodedMsgTargetId',
        templateId: '#decodedMsgTmplId',
        //url: 'http://jl-fitcar-api-test.tmatics.com/v2/device/installation/report',
        url: apiUrl+'/asset/assetLinkmessages',
        params: {
            limit: 10,
            offset: 0,
            searchFor:'',
            searchValue:'',
            sort:[
                {
                    order: -1,
                    columnName:'timestamp'
                }],
            messageType : 'decodedMessage',
            version : 'AssetLink_TIQ-200B'
        },
        searchOptions : {
            enabled : true,
            filterId : "#deco-grid-filter",// Dropdown Filter Id (if available)
            inputId : "#searchDecoMsgVal", // Search Input Id ()
            inputClearId : "#decoClearBtn", // Search Clear button Id ()
            searchId : "#searchDecoMsgBtn", // Search button Id
            autoChange: true, // true - Change Happens based on dropdown change
            enterSearch: true // true - If search while happens while press enter button
        },
        refreshId: "#decodMsgRefresh",
        recperpage: [10, 25, 50,100],
        layout: 'table'
    };
    console.log(tableObj,"=======================");

    var table = new tGrid(tableObj, function (result) {

        $("#totalDecoMsgCount").html(result.data && result.data.data && result.data.data.count ? result.data.data.count : '-' )


    });
}