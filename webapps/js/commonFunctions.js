/**
 * Created by packyag on 15/12/17.
 */
function initNProgress() {
    try {
        $(document).ajaxStart(function () {
            //alert('start');
            NProgress.start();
        }).ajaxError(function () {
            NProgress.done();
        }).ajaxStop(function () {
            NProgress.done();
        });
        NProgress.configure({
            showSpinner: false
        });
        console.log("NProgress init successfully.")
    } catch (e) {
        console.log("NProgress init failed.", e);
    }
}


var fillEmpty = function (data, alternative) {
    if (data == null || data == 'null' || data == 'undefined' || data == undefined || data == '' || data == '-') {
        return alternative ? alternative : ''
    } else {
        return $.trim(data);
    }
};

function isEmpty(data) {
    return (data == null || data == 'null' || data == 'undefined' || data == undefined || data == false || data == 'false' || data == '' || data == '-');
}

function isEmptyObj(data){
    return _.isEmpty(data);
}



var fillEmptyIncZero = function (data, alternative) {

    if (data == 0 || data == null || data == 'null' || data == 'undefined' || data == undefined || data == '' || data == '-') {
        return alternative ? alternative : ''
    } else {
        return $.trim(data);
    }
}

function isEmptyIncZero(data) {
    if (data == 0) {
        return false
    }
    if (data == null || data == 'null' || data == 'undefined' || data == undefined || data == '' || data == '-') {
        return true
    }
    return false;

}


function parseBool(boolValue) {
    return typeof(boolValue) === "string" && boolValue.toLowerCase() === "true" ||
        boolValue === true ||
        boolValue === 1;
}

//function to validate the Email Id
function mailCheck(id) {


    var emailId = $.trim($(id).val().toLowerCase())

    var emailCheck = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    if (!(emailCheck.test(emailId))) {
        $(id).focus();
        return true;
    }
    return false;

}

function mailCheckValidation(id) {
    var emailId = $.trim(id.toLowerCase())

    var emailCheck = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    if (!(emailCheck.test(emailId))) {
        return true;
    }
    return false;

}

function passwordStrengthCheck(id) {
    var password = $.trim($(id).val());
    var capsCheck = /[A-Z]/g,
        numCheck = /[0-9]/g,
    //specialCharCheck = /[-@!$%^&*()_+|~=`{}\[\]:";'<>?,#.\/]/,
        smallCharCheck = /[a-z]/g;

    if (!(capsCheck.test(password)) || !(numCheck.test(password)) || !(smallCharCheck.test(password))) {
        $(id).focus();
        return true;
    }
    return false;


}

//function to validate the password
function passwordCheck(id) {

    var password = $.trim($(id).val());

    //if (!(password.length >= 6 && password.length <= 20 )) {
    if (!(password.length >= 8 )) {
        $(id).focus();
        return true;

    }
    return false;


}
function showAlertCheck(message, alertType, elementId) {
    $('#' + elementId).html('<div id="alertdiv" style="margin-bottom: 0px" class="alert ' + alertType + '"><span style="font-size: 13px;">' + message + '</span></div>')

    setTimeout(function () {
        $("#alertdiv").remove();
    }, 5000)
}

function isDOMEmpty(id) {
    if ($.trim($(id).val()) == "") {
        $(id).focus();
        $(id).addClass('error');
        return true;
    }
    $(id).removeClass('error');
    return false;
}

function convertTimeMilliesToTime(millies, format) {
    if (Number(millies)) {
        //var zone = getUserTimeZone();
        var zone = 'Asia/Calcutta';
        var getFormat = 'MM/DD/YYYY hh:mm:ss A';
        //return moment(Number(millies)).tz(zone).format(getFormat)
        return moment(Number(millies)).format(getFormat)
    } else {
        return false;
    }
}


function daysFromNow(days, format, isBefore) {
    if (isEmptyIncZero(days)) {
        return '';
    }

    if (isBefore) {
        if (moment(moment().subtract(days, 'd').format(format || 'MM/DD/YYYY')).isBefore(isBefore)) {
            return isBefore;
        } else {
            return moment().subtract(days, 'd').format(format);
        }

    } else {
        return moment().subtract(days, 'd').format(format || 'MM/DD/YYYY');
    }
}

