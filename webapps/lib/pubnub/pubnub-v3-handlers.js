/**;
 * Created by Venkatesh on 26-07-2016.
 *
 * PubNub handler for PubNub Version ^3.14.5
 *
 */

var tzlTrack = tzlTrack || '';
var app = app || {};

var TzPubNub = function () { };

TzPubNub.prototype.init = function (options) {

    if (options.enabled) {
        try {
            var UUID = PUBNUB.db.get('session') || (function () {
                    var uuid = PUBNUB.uuid();
                    PUBNUB.db.set('session', uuid);
                    return uuid;
                })();

            //if (tzlTrack) { tzlTrack.trackThis('Pubnub initialisation successful', {session: UUID}, false); }
            console.log("Tzl_PubNub Initialized with UUID = " + UUID);
            app.pubNub = PUBNUB.init({
                publish_key: (options.pubKey || ''),
                subscribe_key: (options.subKey || ''),
                ssl : true,
                uuid: UUID,
                error: function (error) {
                    console.log('Error in Tzl_PubNub Initialisation', error);
                    if (tzlTrack) {
                        tzlTrack.trackThis('Error: Pubnub initialisation failed', {error: error}, false);
                    }
                }
            });

        } catch (e) {
            console.log(e, "Error in Tzl_PubNub Initialisation Try Failed");
            if (tzlTrack) {
                tzlTrack.trackThis('Error: Pubnub initialisation catch error', {error: e}, false);
            }
        }
    } else {
        app.pubNub = null;
        console.log('PubNub Disabled')
    }

};

TzPubNub.prototype.subscribe = function (channel, callback) {
    try {
        if (app.pubNub && channel) {
            
            if(!callback || typeof callback != 'function'){
                console.error("Callback function is not defined in Tzl PubNub Subscribe");
                return;
            }
            app.pubNub.subscribe({
                channel: channel,
                message: function (message, env, channel) {
                    callback(message, env);
                },
                connect: function () {
                    console.log("Connected to the channel - " + channel);
                },
                disconnect: function () {
                    console.log("Disconnected from the channel - " + channel);
                },
                reconnect: function () {
                    console.log("Reconnected to the channel- " + channel);
                },
                error: function () {
                    console.log("Network Error for the channel - " + channel);
                }
            })
        } else {
            if (tzlTrack) {
                //tzlTrack.trackThis('err_in_tzl_pubnub_subscribe_pubnub_or_channel_not_found', '', false);
            }
            console.log('Subscribe Failed - Tzl_PubNub or Channel Not Found',app.pubNub,channel);

        }
    } catch (e) {
        if (tzlTrack) {
            //tzlTrack.trackThis('err_in_tzl_pubnub_subscribe', {error:e}, false);
        }
        console.log(e, 'Error in Tzl_PubNub Subscribe');
    }
};


TzPubNub.prototype.publish = function (channel, message, callback) {
    try {
        if (app.pubNub && channel) {
            app.pubNub.publish({
                channel : channel,
                message : message,
                callback : function(m){
                    console.log(m);
                    callback(m);
                }
            });
        } else {
            if (tzlTrack) {
                //tzlTrack.trackThis('err_in_tzl_pubnub_subscribe_pubnub_or_channel_not_found', '', false);
            }
            console.log('Subscribe Failed - Tzl_PubNub or Channel Not Found',app.pubNub,channel);

        }
    } catch (e) {
        if (tzlTrack) {
            //tzlTrack.trackThis('err_in_tzl_pubnub_subscribe', {error:e}, false);
        }
        console.log(e, 'Error in Tzl_PubNub Subscribe');
    }
};





TzPubNub.prototype.unSubscribe = function (channel) {
    try {
        if (app.pubNub && channel) {
            app.pubNub.unsubscribe({
                channel: channel
            },function (e) {
                if(e.status == 200){
                    //tzlTrack.trackThis('Un subscribing pubnub successful', {error:e}, false);
                    console.log("UnSubscribed from " + channel);
                }else{
                    console.log("UnSubscribed failed for channel - " + channel);
                    if (tzlTrack) {
                        tzlTrack.trackThis('Error: Un subscribing pubnub failed', {error:e}, false);
                    }
                }
            })
        } else {
            if (tzlTrack) {
                //tzlTrack.trackThis('err_in_tzl_pubnub_un_subscribe_pubnub_or_channel_not_found', '', false);
            }
            console.log('UnSubscribe Failed - Tzl_PubNub or Channel Not Found');
        }
    } catch (e) {
        if (tzlTrack) {
            //tzlTrack.trackThis('err_in_tzl_pubnub_un_subscribe', {error:e}, false);
        }
        console.log(e, 'Error in Tzl_PubNub Subscribe');
    }
};



tzlPubNub = new TzPubNub();

