/**
 * Created by packyag on 13/12/17.
 */

var tzutils = null;
var Utils = function(){};

Utils.prototype.isDOMEmpty = function(id){

    if($.trim($(id).val()) == ""){
        $(id).focus();
        $(id).addClass('error');
        return true;
    }
    $(id).removeClass('error');
    return false;
};

Utils.prototype.showAlert = function (message,alertType,elementId){
    $('#'+elementId).html('<div id="alertdiv" class="alert ' +  alertType + '"><span style="font-size: 15px">'+message+'</span></div>')

    setTimeout(function(){
        $("#alertdiv").remove();
    },5000)
};

Utils.prototype.showFeedbackAlert = function (message,alertType,elementId){
    $(elementId).html('<div id="alertdiv" class="alert ' +  alertType + '"><span style="font-size: 15px">'+message+'</span></div>')

    setTimeout(function(){
        $("#alertdiv").remove();
    },5000)
};

Utils.prototype.showAlertMsg = function (message,alertType,elementId){

    $('#'+elementId).html('<div id="alertdiv" class="alert ' +  alertType + '"><span style="font-size: 15px">'+message+'</span></div>')


};

Utils.prototype.hideAlert = function (){
    $("#alertdiv").remove();

};

// function to set the page height .
Utils.prototype.setDocumentHeight = function(header,footer,body){

    var windowHeight = $(window).height();
    var headerHeight = $(header).outerHeight(false);
    var footerHeight = $(footer).outerHeight(false);
    $(body).css('min-height',windowHeight - (headerHeight +footerHeight));

};
//// Function that validates email address through a regular expression.
Utils.prototype.validateEmail = function (emailId){

    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailId);
};

// function to get the encoded string
Utils.prototype.encodeStringFromJSON = function (input){

    if(typeof  input == 'object'){
        return  encodeURIComponent(JSON.stringify(input).replace(/&/, "&amp;").replace(/'/g, "\\"));
    }


}

// function to get the decoded string
Utils.prototype.decodeString = function (inputStr){

    try{
        return  decodeURIComponent(inputStr).replace(/&amp;/, "&").replace(/\\/g, "'");
    }catch (e){
        console.log("Error in decodeString " +e)
    }
}

// function to get the decoded and parsed output ;
Utils.prototype.stringDecodedParse = function (inputStr){

    try{
        return  JSON.parse(decodeURIComponent(inputStr).replace(/&amp;/, "&").replace(/\\/g, "'")) ;
    }catch (e){
        console.log("Error in stringDecodedParse" +e)
    }
}

// function to capitalize the string input ;
Utils.prototype.capitalize = function (string){

    return string.replace( /(^|\s)([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase()}); // this capitalize first letter of each word

}

// function to fadeIn fadeOut effect ;
Utils.prototype.fadeInOutEffect = function (el,timeout,timein,isLoop){

    var $el = $(el),intId,fn = function(){
        $el.fadeOut(timeout).fadeIn(timein);
    };
    fn();
    if(isLoop){
        intId = setInterval(fn,timeout+timein+100);
        return intId;
    }
    return false;

}

// function to get UnOrderedList Html
Utils.prototype.getUnOrderedListHtml = function (inputArray,ulclass){

    var self = this;

    ulclass = ulclass || 'utils-ul';

    if((self.getType(inputArray) === 'array' ) &&  inputArray.length > 0){

        var elm = '<ul class="'+ulclass+'">';
        $.each(inputArray, function (i, val) {
            elm += '<li>' + val + '</li>';

        });
        elm += '</ul>';
        return elm;

    }
    else{
        console.error("input is not a valid array or empty array");
        return '';
    }


}

// Function to get the type of input (string || array || object)
Utils.prototype.getType = function (input){

    if (Array.isArray(input)) return 'array';
    else if (typeof input == 'string') return 'string';
    else if (input != null && typeof input == 'object') return 'object';
    else return 'other';
}

// function to equalize hight of elements (input : classnames)
Utils.prototype.equalizeHeight = function(element){

    var maxHeight = 0;


    $(element).each(function(){
        if ($(this).innerHeight() > maxHeight) {
            maxHeight = $(this).innerHeight();
        }
        console.log("max height",maxHeight,element);

    });

    $(element).css('min-height',maxHeight);

};

// fill alternative for empty fields
Utils.prototype.fillEmpty = function(data, alternative){

    if(data == null || data == 'undefined' || data == undefined || data == ''){
        return alternative ? alternative : ''
    }else {
        return $.trim(data);
    }
}

// round value to the given decimals
function roundTo(value, decimals) {
    if (isEmpty(value) != 0) {
        var decimalValue = isEmpty(decimals);
        return Math.round(value * Math.pow(10, decimalValue))/ Math.pow(10, decimalValue);
    } else {
        return 0;
    }
}


// fill alternative for empty fields
Utils.prototype.isEmpty = function(data){

    if(data == null || data == 'null' || data == 'undefined' || data == undefined || data == '' || data == '-') {
        return true
    }
    return false;
}

// function to validate a string having whitespace or not
Utils.prototype.noWhiteSpace = function (value) {

    return value.indexOf(" ") < 0 && value != "";
}

tzutils = new Utils();


