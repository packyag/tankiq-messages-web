/**
 * Created by packyag on 13/4/17.
 */
var tzlTrack = null;

// Types of Analytics
var GOOGLE_A = "Google";
var MIXPANEL_A = "MixPanel";
var GOOGLE_A_MIXPANEL = "GoogleWithMixPanel";

// Key token for analytics
//var googleAnalyticsKey = "UA-83524727-1";

// :TODO Choose Venky Account for Production Release (must)
//var mixPanelAnalyticsKey= ""; // Debug (SaNa Account New)


//:TODO Check this line before Production Release (must)
//var analyticsType = "";
//var analyticsType = GOOGLE_A;
var analyticsType = MIXPANEL_A;
//var analyticsType = GOOGLE_A_MIXPANEL;


// Flag for mixpanel user identification
var userIdentifyStatus = false;

var tFields    = null,
    tHitTypes  = null,
    tLogLevel  = null;

var TzTrack = function(){
    this.isInitiated = false;

};

// Get Google Analytics Plugin
var getGAPlugin = function() {

    var analytics = navigator.analytics;
    if (analytics) {

        tFields    = analytics.Fields;
        tHitTypes  = analytics.HitTypes;
        tLogLevel = analytics.LogLevel;

//        console.log('GA Plugin found');
        return analytics;
    }
    console.log('GA Plugin NOT found');
};

var gaInitializeSuccess = function(result) {
    console.log('Analytics initialized: ' + result);
};

var gaInitializeError = function(error) {
    console.log('Error in Initialize : ' + error);
};

var gaSuccessHandler = function(result) {
    console.log('Tracking Success: ' +result);
};

var gaErrorHandler = function(error) {
    console.log('Tracking Error: ' + error);
};

var analyticsHandler = function(msg) {
//    console.log(msg);
};

// Function to get the type of input (string || array || object)
var getType  = function(input){

    if (Array.isArray(input)) return 'array';
    else if (typeof input == 'string') return 'string';
    else if (input != null && typeof input == 'object') return 'object';
    else return 'other';

};

// Function to init Analytics
TzTrack.prototype.initAnalytics = function (options){

    var self = this;
    try{
        var googleAnalyticsKey,mixPanelAnalyticsKey = null;
        var mixpanelEnabled = false,gaEnabled = false;

        if(options){
            if(options.googleAnalytics && options.googleAnalytics.apiKey){
                googleAnalyticsKey = options.googleAnalytics.apiKey;
                gaEnabled = options.googleAnalytics.enabled;
            }

            if(options.mixpanel){
                mixPanelAnalyticsKey = options.mixpanel.apiKey;
                mixpanelEnabled = options.mixpanel.enabled;
            }
        }

        if(analyticsType == GOOGLE_A && (gaEnabled)){
            console.log('Initialize GA');
            if (getGAPlugin()) {
                getGAPlugin().setTrackingId(googleAnalyticsKey, gaInitializeSuccess, gaInitializeError);
            }
        }
        else if(analyticsType == MIXPANEL_A && mixpanelEnabled){
            console.log('Initialize MixPanel');
            mixpanel.init(mixPanelAnalyticsKey);

            self.isInitiated = true;
        }
        else if(analyticsType == GOOGLE_A_MIXPANEL  && (gaEnabled && mixpanelEnabled)){
            console.log("Initialize MixPanel and Google Analytics");

            // initialize google analytics
            if (getGAPlugin()) {
                getGAPlugin().setTrackingId(googleAnalyticsKey, gaInitializeSuccess, gaInitializeError);
            }

            // initialize Mixpanel
            mixpanel.init(mixPanelAnalyticsKey);
        }
        else{
            analyticsHandler('No analytics enabled');
        }
    }catch (e){
        console.log(e);
    }
};

// Tracking a page view ( isEvent ? false) or event (isEvent ? true).
TzTrack.prototype.trackThis = function(pageOrEventName, object, isEvent){

    //console.log("%c Mixpanel track : " +pageOrEventName,'color: #298941');

    try{
        if(analyticsType == GOOGLE_A){
            if(!isEvent){
                // To track Page
                if(object){
                    this.sendScreenView(pageOrEventName,object);
                }
                else{
                    this.sendScreenView(pageOrEventName);
                }
            }
            else if(isEvent){
                if(getType(object) =='object'){
                    console.log("trackEvent: " + pageOrEventName+':'+JSON.stringify(object)); // Eg. Button Clicked

                }
                if(object){
                    this.sendEvent(null,pageOrEventName,object);
                }
                else{
                    this.sendEvent(null,pageOrEventName);
                }
            }
        }
        else if(analyticsType == MIXPANEL_A){

            var event_name = pageOrEventName,
                properties = object;

//            console.log('MixPanel track event :'+event_name);

            mixpanel.track(event_name,properties);
        }
        else if(analyticsType == GOOGLE_A_MIXPANEL){
            if(!isEvent){
                // To track Page
                if(object){
                    this.sendScreenView(pageOrEventName,object);
                }
                else{
                    this.sendScreenView(pageOrEventName);
                }
            }
            else if(isEvent){
                console.log("trackEvent: " + pageOrEventName+':'+JSON.stringify(object)); // Eg. Button Clicked
                if(object){

                    this.sendEvent(null,pageOrEventName,object);
                }
                else{
                    this.sendEvent(null,pageOrEventName);

                }
            }
            var event_name = pageOrEventName,
                properties = object;
//            console.log('MixPanel track event :'+event_name);

            mixpanel.track(event_name,properties);
        }
        else{
            analyticsHandler('No analytics enabled');
        }
    }catch (e){
//        console.log(e);
    }
}

// For google analytics only
TzTrack.prototype.sendEvent = function(category,action,params){
    try{
        var category = category ? category :'AppEvent' ;
        if(params && getType(params) =='object'){
            if (getGAPlugin()) {

                getGAPlugin().sendEventWithParams(category, action, null, null,params,gaSuccessHandler, gaErrorHandler);
            }
        }else{
            if (getGAPlugin()) {
                getGAPlugin().sendEvent(category, action, null, null,gaSuccessHandler, gaErrorHandler);
            }
        }
    }catch (e){
        console.log(e);
    }
}

// For google analytics only
TzTrack.prototype.sendScreenView = function(screenName, params){
    try{

        screenName = screenName.toString();

        if(params){
            if (getGAPlugin()) {
                getGAPlugin().sendAppViewWithParams(screenName, params,gaSuccessHandler, gaErrorHandler);
            }
        }else{
            if (getGAPlugin()) {
                getGAPlugin().sendAppView(screenName,gaSuccessHandler, gaErrorHandler);
            }
        }
    }
    catch (e){
        console.log(e);
    }
}

// Function to track the user
TzTrack.prototype.trackUser = function(userId,userObject){

//    console.log("userId: " + userId);
    try{
        if(userId!= null && userId!='') {
            if (analyticsType == GOOGLE_A) {
                if (getGAPlugin()) {
                    getGAPlugin().set("&uid",userId, gaSuccessHandler, gaErrorHandler);

                    //                userObject[Fields.HIT_TYPE] = HitTypes.EVENT;
                    userObject[tFields.EVENT_CATEGORY] = 'UX';
                    userObject[tFields.EVENT_ACTION] = 'Sign in';
                    this.send(userObject);
                }
            }
            else if (analyticsType == MIXPANEL_A) {
                mixpanel.identify(userId);
                userIdentifyStatus = true;
                this.setPeopleObject(userObject);
            }
            else if(analyticsType == GOOGLE_A_MIXPANEL){
                //
                if (getGAPlugin()) {
                    getGAPlugin().set("&uid",userId, gaSuccessHandler, gaErrorHandler);

//                  userObject[Fields.HIT_TYPE] = HitTypes.EVENT;
                    userObject[tFields.EVENT_CATEGORY] = 'UX';
                    userObject[tFields.EVENT_ACTION] = 'Sign in';
                    this.send(userObject);
                }
                mixpanel.identify(userId);
                userIdentifyStatus = true;
                this.setPeopleObject(userObject);
            }
            else{
                analyticsHandler('No analytics enabled');
            }
        }
    }
    catch (e){
        console.log(e);
    }
};

TzTrack.prototype.timeEvent = function(event){
    try {
        mixpanel.time_event(event);
    } catch (e) {
        console.log(e);
    }

};




// Generates a hit to be sent with the specified params and current field values // For GA only
TzTrack.prototype.send = function(object) {
    try{
        if (getGAPlugin()) {
            getGAPlugin().send(object, gaSuccessHandler, gaErrorHandler);
        }
    }
    catch(e){
        console.log(e);
    }
};


// Sets a field
//
//  name        - String    (required)
//  value       - String    (optional) use null to unset a field
//  For GA only
TzTrack.prototype.setField = function(name, value) {
    try{
        if (getGAPlugin()) {
            getGAPlugin().set(name, value, gaSuccessHandler, gaErrorHandler);
        }
    }
    catch (e){
        console.log(e);
    }
};

// For Mixpanel only
TzTrack.prototype.setPeopleObject = function(params){
//    console.log("peopleObject: " + JSON.stringify(params));
    try{
        if(analyticsType == MIXPANEL_A){
            // identify must be called along with people.set
            if(userIdentifyStatus){
                mixpanel.people.set(params);
            }
        }
        else if(analyticsType == GOOGLE_A_MIXPANEL) {

            // identify must be called along with people.set
            if (userIdentifyStatus) {
                mixpanel.people.set(params);
            }
        }
        else{
            analyticsHandler('No analytics enabled');
        }
    }
    catch (e){
        console.log(e);
    }
}

// For Mixpanel only
TzTrack.prototype.register = function(params){
    try{
        if(analyticsType == MIXPANEL_A){
            mixpanel.register(params);
        }
        else if(analyticsType == GOOGLE_A_MIXPANEL) {

        }
        else{
            analyticsHandler('No analytics enabled');
        }
    }
    catch (e){
        console.log(e);
    }
}


// For Mixpanel only
TzTrack.prototype.registerOnce = function(params){
    try{
        if(analyticsType == MIXPANEL_A){
            mixpanel.register_once(params);
        }
        else if(analyticsType == GOOGLE_A_MIXPANEL) {

        }
        else{
            analyticsHandler('No analytics enabled');
        }
    }
    catch (e){
        console.log(e);
    }
}

// For Mixpanel only
TzTrack.prototype.unregister = function(propertyName){
    try{
        if(analyticsType == MIXPANEL_A){
            mixpanel.unregister(propertyName);
        }
        else if(analyticsType == GOOGLE_A_MIXPANEL) {

        }
        else{
            analyticsHandler('No analytics enabled');
        }
    }
    catch (e){
        console.log(e);
    }
};


// For Mixpanel only
TzTrack.prototype.aliasId = function(id){
    try{
        if(analyticsType == MIXPANEL_A){
            mixpanel.alias(id);
        }
        else if(analyticsType == GOOGLE_A_MIXPANEL) {

        }
        else{
            analyticsHandler('No analytics enabled');
        }
    }
    catch (e){
        console.log(e);
    }
}




// Closes the the tracker //  For GA only
TzTrack.prototype.closeTracker = function(){
    try{
        if(analyticsType == MIXPANEL_A){
            // identify must be called along with people.set
            mixpanel.disable();
        }
        else if(analyticsType == GOOGLE_A_MIXPANEL) {
            //
            if (getGAPlugin()) {
                getGAPlugin().close(gaSuccessHandler,gaErrorHandler);
            }
        }
        else{
            analyticsHandler('No analytics enabled');
        }
    }
    catch (e){
        console.log(e);
    }
}


// DisableTracker the the tracker //  For GA only
TzTrack.prototype.disableTracker = function(){
    try{
        if(analyticsType == MIXPANEL_A){
//            mixpanel.disable();
            mixpanel.register({"$ignore":"true"});
        }
        else if(analyticsType == GOOGLE_A_MIXPANEL) {
            //
            if (getGAPlugin()) {
                getGAPlugin().close(gaSuccessHandler,gaErrorHandler);
            }
        }
        else{
            analyticsHandler('No analytics enabled');
        }
    }
    catch (e){
        console.log(e);
    }
}

TzTrack.prototype.getDistinctId = function(){
    try {
        if(analyticsType == MIXPANEL_A){
            return mixpanel.get_distinct_id() || null;
        }
    } catch (e) {
        console.log(e);
    }

    return null;
};

tzlTrack = new TzTrack();
