/**
 * Created by thilagam on 28/9/15.
 */
var conf = {
    web: {
        host: "0.0.0.0",
        port: "9001",
        method: "session",
        views: {},
        static: {}
    },
    database: {
        api: 'mongodb',
        host: '10.1.10.47',
        port: '12345',
        schema: 'teezle_web',
        auth: false,
        username: '',
        password: ''
    },

    session: {
        store: 'inmemory', // redis | mongo | inmemory //TODO mongo not implemented
        host: '10.1.10.47',
        port: '6379'
    },
    mapKey :"AIzaSyD2iBxqvigy_WDahAwkKYgB_GEgq8AO0CY",
    //apiUrl :"http://10.1.10.54:8008"
    apiUrl :"http://184.169.130.53:8008"


};

module.exports = conf;
