
var uiAction = require('../controller/ui-action.js');
var _ = require('underscore')


var UIRoutes = function (app) {
    this.app = app;
    this.conf=app.conf;
    this.actionInstance = new uiAction(app);
};

module.exports = UIRoutes;

UIRoutes.prototype.init = function () {

    var app = this.app;
    var conf = this.conf;
    var isAuthorized = app.sessionCheck;
    var actionInstance = this.actionInstance;

    /*-------------------------- UI Routes ----------------------------*/

    //Init Routes
    app.get("/", function (req, res) {
        actionInstance.getLoginPage(req, res);
    });
    app.get("/login", function (req, res) {
        actionInstance.getLoginPage(req, res);
    })
    ;app.get("/loginSubmit", function (req, res) {
        actionInstance.getLoginSubmitPage(req, res);
    });
     app.get("/reports", function (req, res) {
        actionInstance.getStatusPage(req, res);
    });


    //SAMPLE GRID PAGE


    app.get("/v1/status/report", function (req, res) {
        actionInstance.getStatusGridData(req, res);
    });

}