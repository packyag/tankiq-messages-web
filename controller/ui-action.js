var _ = require("underscore");

var  page;
var homeLayout = "layout/home-layout.html";
var statusGridData = require('../object/statusGridData.json');
var accountList = require('../object/accountList.json');
var assetList = require('../object/assetList.json');


var UIAction = function (app) {
    this.app = app;
    this.conf = app.conf;
};

module.exports = UIAction;

// Index page
UIAction.prototype.getIndexPage = function(req,res){

    var reqUrl = req.headers.host;
    res.redirect('/status');
};
UIAction.prototype.getLoginPage = function(req,res){
    page = 'login';

 var reqUrl = req.headers.host;
    res.render("login.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl , page : page });

};
UIAction.prototype.getLoginSubmitPage = function(req,res) {

    var reqUrl = req.headers.host;
    console.log(req.query.email);
    if (req.query.email == 'tzuser' && req.query.pwd == '2weeks') {
        res.redirect("/status")
    }
    else
    {
        res.redirect("/login")
    }



};


UIAction.prototype.getStatusGridData = function(req,res){
    var reqUrl = req.headers.host;
    res.send(statusGridData);

};

UIAction.prototype.getStatusPage = function(req,res){
    page = 'status';

    var   temperature = 'F';
    var reqUrl = req.headers.host;
    res.render("report/reports.html", {layout: homeLayout,conf: this.conf, branding: this.conf.branding, reqUrl: reqUrl , page : page , accounts : accountList , assets : assetList });

};
